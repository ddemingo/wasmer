# wasm

## wasmer

https://wasmer.io/


sudo apt install libtinfo5
curl https://get.wasmer.io -sSfL | sh


## wat

```shell
sudo apt update && sudo apt install build-essential
git clone --recursive https://github.com/WebAssembly/wabt
cd wabt
git submodule update --init
mkdir build
cd build
cmake ..
cmake --build .
```

wat2wasm example.wat
